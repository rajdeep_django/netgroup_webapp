//bootstrap tooltip//
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
//bootstrap tooltip//


/*==owl-carousel single slider js==*/
$('#slider').owlCarousel({
  loop:true,
  autoplayHoverPause:true,
  navText : ["<img src='images/lft-arrow.png'>","<img src='images/rgt-arrow.png'>"],
  nav:false,
  dots:true,
  autoplay:5000,
  smartSpeed: 1950,
  responsive:{
      0:{
          items:1
      },
      600:{
          items:1
      },
      1000:{
          items:1
      }
  }
});
/*==/owl-carousel single slider js==*/


/*==testimonials slider js==*/
$('#testimonials').owlCarousel({
  loop:true,
  autoplayHoverPause:true,
  navText : ["<img src='images/lft-arrow.png'>","<img src='images/rgt-arrow.png'>"],
  nav:true,
  dots:false,
  autoplay:5000,
  smartSpeed: 1950,
  responsive:{
      0:{
          items:1
      },
      600:{
          items:1
      },
      1000:{
          items:1
      }
  }
});
/*==/testimonials slider js==*/


/*==blog page slider js==*/
$('.blog-carousel').owlCarousel({
  loop:true,
  navText : ["<i class='fa fa-long-arrow-left'></i>","<i class='fa fa-long-arrow-right'></i>"],
  nav:true,
  dots:false,
  autoplay:6000,
  smartSpeed: 1950,
  responsive:{
      0:{
          items:1
      },
      600:{
          items:1
      },
      1000:{
          items:3
      }
  }
})
/*==/blog page slider js==*/



/*==accordion script==*/
const nanoExpansion = new NanoExpansion({
      duration: 0.2,
      wrapper: "#myexpansion",
      autoFolding: false,
    });
/*==/accordion script==*/


//aos animation js// 
  // $(window).on("load", function(){
    // AOS.init({
     // easing: 'ease-in-out-sine'
    // });
  // });
//aos animation js// 


//===== Mobile Menu =============================//
$(".navbar-toggler").on('click', function() {
	$(this).toggleClass("active");
});


var subMenu = $(".sub-menu-bar .navbar-nav .sub-menu");

if(subMenu.length) {
	subMenu.parent('li').children('a').append(function () {
		return '<button class="sub-nav-toggler"> <i class="fa fa-chevron-down"></i> </button>';
	});
	
	var subMenuToggler = $(".sub-menu-bar .navbar-nav .sub-nav-toggler");
	
	subMenuToggler.on('click', function() {
		$(this).parent().parent().children(".sub-menu").slideToggle();
		return false
	});
}
//===== /Mobile Menu end=============================//


// smooth scrool bottom to top js //
$(document).ready(function(){

    // hide #back-top first
    $("#back-top").hide();
    
    // fade in #back-top
    $(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('#back-top').fadeIn();
            } else {
                $('#back-top').fadeOut();
            }
        });

        // scroll body to 0px on click
        $('#back-top a').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 1500);
            return false;
        });
    });

});
// smooth scrool bottom to top js end//


/*==counter js start==*/
(function ($) {
	$.fn.countTo = function (options) {
		options = options || {};
		
		return $(this).each(function () {
			// set options for current element
			var settings = $.extend({}, $.fn.countTo.defaults, {
				from:            $(this).data('from'),
				to:              $(this).data('to'),
				speed:           $(this).data('speed'),
				refreshInterval: $(this).data('refresh-interval'),
				decimals:        $(this).data('decimals')
			}, options);
			
			// how many times to update the value, and how much to increment the value on each update
			var loops = Math.ceil(settings.speed / settings.refreshInterval),
				increment = (settings.to - settings.from) / loops;
			
			// references & variables that will change with each update
			var self = this,
				$self = $(this),
				loopCount = 0,
				value = settings.from,
				data = $self.data('countTo') || {};
			
			$self.data('countTo', data);
			
			// if an existing interval can be found, clear it first
			if (data.interval) {
				clearInterval(data.interval);
			}
			data.interval = setInterval(updateTimer, settings.refreshInterval);
			
			// initialize the element with the starting value
			render(value);
			
			function updateTimer() {
				value += increment;
				loopCount++;
				
				render(value);
				
				if (typeof(settings.onUpdate) == 'function') {
					settings.onUpdate.call(self, value);
				}
				
				if (loopCount >= loops) {
					// remove the interval
					$self.removeData('countTo');
					clearInterval(data.interval);
					value = settings.to;
					
					if (typeof(settings.onComplete) == 'function') {
						settings.onComplete.call(self, value);
					}
				}
			}
			
			function render(value) {
				var formattedValue = settings.formatter.call(self, value, settings);
				$self.html(formattedValue);
			}
		});
	};
	
	$.fn.countTo.defaults = {
		from: 0,               // the number the element should start at
		to: 0,                 // the number the element should end at
		speed: 1000,           // how long it should take to count between the target numbers
		refreshInterval: 100,  // how often the element should be updated
		decimals: 0,           // the number of decimal places to show
		formatter: formatter,  // handler for formatting the value before rendering
		onUpdate: null,        // callback method for every time the element is updated
		onComplete: null       // callback method for when the element finishes updating
	};
	
	function formatter(value, settings) {
		return value.toFixed(settings.decimals);
	}
}(jQuery));

jQuery(function ($) {
  // custom formatting example
  $('.count-number').data('countToOptions', {
	formatter: function (value, options) {
	  return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
	}
  });
  
  // start all the timers
  $('.timer').each(count);  
  
  function count(options) {
	var $this = $(this);
	options = $.extend({}, options || {}, $this.data('countToOptions') || {});
	$this.countTo(options);
  }
});
/*==/counter js end==*/


//===== accordian js start=============================//
const accordion = document.querySelector('.accordion');
const items = accordion.querySelectorAll('.accordion__item');

items.forEach((item) => {
  const title = item.querySelector('.accordion__title');
  
  title.addEventListener('click', (e) => {
    const opened_item = accordion.querySelector('.is-open');
    
    // Toggle current item
    toggle_item(item); 
    
    // Close earlier opened item if exists
    if (opened_item && opened_item !== item) {
      toggle_item(opened_item);
    }
    
    
  });
});

const toggle_item = (item) => {
  const body = item.querySelector('.accordion__body');
  const content = item.querySelector('.accordion__content');
        
  if (item.classList.contains('is-open')) {
    body.removeAttribute('style');
    item.classList.remove('is-open');
  }else {
    body.style.height = body.scrollHeight + 'px';
    item.classList.add('is-open');
  }
}
 //===== /accordian js end=============================// 
 
 